import java.io.*;
class EParser	{
	static int lookahead;
	public EParser() throws IOException {
		lookahead = System.in.read();
	}

	private boolean is(int val)	{
		return val == lookahead;
	}
	void match(int t)	throws IOException {
		if (t == lookahead) lookahead = System.in.read();
		else throw new Error("Syntax Error");
	}

	void factor()	throws IOException {
		if (Character.isDigit((char)lookahead))	{
			System.out.write((char)lookahead); match(lookahead);
		} else if (is('('))	{
			match('('); expr(); match(')');
		} else throw new Error("Syntax Error");
	}

	void term() throws IOException {
		factor();
		while (true)	{
			if (is('*'))	{
				match('*'); factor(); System.out.write('*');
			} else if (is('/'))	{
				match('/'); factor(); System.out.write('/');
			} else break;
		}
	}

	void expr() throws IOException {
		term();
		while (true)	{
			if (is('+'))	{
				match('+'); term(); System.out.write('+');
			} else if (is('-'))	{
				match('-'); term(); System.out.write('-');
			} else break;
		}
	}

	public static void main(String [] args)	{
		try {
			EParser parser1 = new EParser();
			parser1.expr();
		} catch (Exception e)	{
			System.out.println(e);
		} finally {
			System.out.write('\n');
		}
	}
}