class Complement {
	private int lookahead = -1;
	private boolean is(int val)	{
		return lookahead == val;
	}

	public void stateChange()	{
		int state = 0;
		int acceptcode = -1;
		boolean loopIsValid = true;
		do {
 			//lookahead= read(); You must update lookahead here
			switch (state) {
				case 0 :
					if (is('~')) state = 1;
					else loopIsValid = false; 
					break;
				case 1 :
					state = 2;
					acceptcode = 1; 
					break;

			} 
		} while(loopIsValid && state != 2);
	}
}
