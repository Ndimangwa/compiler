class And {
	private int lookahead = -1;
	private boolean is(int val)	{
		return lookahead == val;
	}

	public void stateChange()	{
		int state = 0;
		int acceptcode = -1;
		boolean loopIsValid = true;
		do {
 			//lookahead= read(); You must update lookahead here
			switch (state) {
				case 0 :
					if (is('&')) state = 1;
					else loopIsValid = false; 
					break;
				case 1 :
					if (is('&')) state = 2;
					else if (is('=')) state = 4;
					else state = 5;
					acceptcode = 1; 
					break;
				case 2 :
					if (is('=')) state = 3;
					else state = 5;
					acceptcode = 2; 
					break;
				case 4 :
					state = 5;
					acceptcode = 3; 
					break;
				case 3 :
					state = 5;
					acceptcode = 4; 
					break;

			} 
		} while(loopIsValid && state != 5);
	}
}
