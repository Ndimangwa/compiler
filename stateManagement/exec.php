<?php 
require_once("class/StateGen.php");
if ((sizeof($argv) == 2) && ($argv[1] == "-h" || $argv[1] == "--help")) {
    echo "\n**************************************************************************************************************************";
    echo "\n***********************************************JAVA STATE*****************************************************************";
    echo "\n** This program will read a data file which contains representation of the state diagram, the following is the format   **";
    echo "\n** of the data file CURRENT-STATE,INPUT-SYMBOL,NEXT-STATE,IS-ACCEPT-STATE                                               **";
    echo "\n**                                                                                                                      **";
    echo "\n** Output of the program will be a java file, with name as provided in a --class                                        **";
    echo "\n**                                                                                                                      **";
    echo "\n** SYNTAX: php exec.php --input <input-file> --class <java-class-name> --method <java-method-name>                      **";
    echo "\n**                                                                                                                      **";
    echo "\n** -i, --input              : Input state diagram file                                                                  **";
    echo "\n** -c, --class              : Name of the output Java class and filename                                                **";
    echo "\n** -m, --method             : Name of the output Java method                                                            **";
    echo "\n**                                                                                                                      **";
    echo "\n** EXAMPLE: php exec.php --input myfile.dt --class Lexical --method lessThan                                            **";
    die("\n**************************************************************************************************************************\n");
}
if (sizeof($argv) < 3) die("\nTry: php exec.php --help\n"); 
$filename = null;
$classname = null;
$methodname = null;

//Extracting data
for ($i = 1; $i < sizeof($argv) - 1; $i = $i + 2)  {
    $control = $argv[$i];
    $data = $argv[$i+1];

    if (is_null($filename) && ($control == "-i" || $control == "--input"))  {
        $filename = $data;
    } else if (is_null($classname) && ($control == "-c" || $control == "--class"))  {
        $classname = $data;
    } else if (is_null($methodname) && ($control == "-m" || $control == "--method"))    {
        $methodname = $data;
    }
}

if (is_null($filename)) die("\nFilename not set\n");
if (is_null($classname)) $classname = "DefaultStateManager";
if (is_null($methodname)) $methodname = "stateChange";

try {
    $javacode = StateGen::generate($filename, $classname, $methodname);
    //Preparing filename
    $filename = dirname($filename).DIRECTORY_SEPARATOR.$classname.".java";
    if (! file_put_contents($filename, $javacode)) throw new Exception("Could not Save Contents");

} catch (Exception $e)  {
    die($e->getMessage());
}
?>