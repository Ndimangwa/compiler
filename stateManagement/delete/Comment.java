class Comment {
	private int lookahead = -1;
	private int acceptcode = -1;
	private boolean is(int val)	{
		return lookahead == val;
	}

	public void comments()	{
		int state = 0;
		boolean loopIsValid = true;
		do {
 			//lookahead= read(); You must update lookahead here
			switch (state) {
				case 0 :
					if (is('/')) state = 1;
					else loopIsValid = false; 
					break;
				case 1 :
					if (is('/')) state = 2;
					else state = 3;
					break;
				case 2 :
					if (is('\n')) state = 7;
					else state = 2;
					break;
				case 7 :
					state = 6;
					acceptcode = 1; 
					break;
				case 3 :
					if (is('*')) state = 4;
					else state = 3;
					break;
				case 4 :
					if (is('*')) state = 4;
					else if (is('/')) state = 5;
					else state = 3;
					break;
				case 5 :
					state = 6;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 6);
	}
}
