<?php 
class StateGen	{
	/*
	Generated code must have a function called is 
	$lookahead 
	*/
	private static $_INITIAL_FIN_STATE = -1;
	public static function generate($filename, $classname = "DefaultStateManager", $methodname = "stateChange")	{
		$file1 = fopen($filename, "r") or die("Could not open file [$filename] for reading");
		//moving line by line
		$finishState = self::$_INITIAL_FIN_STATE;
		$currentStateTable = array();
		$lineno = 0;
		while (($line = fgets($file1)) !== false)	{
			$lineno++;
			$line = trim($line);
			if ($line == "") continue;
			$lineArr = explode(",", $line);
			if (sizeof($lineArr) != 4) continue;

			$currentState = trim($lineArr[0]);
			$inputSymbol = trim($lineArr[1]);
			$nextState = trim($lineArr[2]);
			$isAcceptState = trim($lineArr[3]) == "1";

			//Since we have a common finishState, we should disallow multiple finish state
			if ($isAcceptState && ! ($finishState == self::$_INITIAL_FIN_STATE) && ! ($finishState == $nextState)) die("Error at Line Number $lineno: Multiple Finish States found. Allowed to have only one finish state");
			if ($isAcceptState) $finishState = $nextState;

			if (! isset($currentStateTable[$currentState])) $currentStateTable[$currentState] = array();

			if ($inputSymbol == "***") $currentStateTable[$currentState][sizeof($currentStateTable[$currentState])] = "state = $nextState";
			else	$currentStateTable[$currentState][sizeof($currentStateTable[$currentState])] = "if (is('$inputSymbol')) state = $nextState";
		}
		fclose($file1);	
		//Now we have string starting with if and the rest with no else
		$dataOutput = "";
		$acceptStateCode = 0;
		$acceptStateText = "state = $finishState";
		$acceptStateTextLength = strlen($acceptStateText);
		foreach ($currentStateTable as $currentState => $currentStateTableBlock)	{
			$len = sizeof($currentStateTableBlock);
			$dataOutput .= "\t\t\t\tcase $currentState :\n";
			$textdata = $currentStateTableBlock[0];
			if ($len == 1)	{
				$dataOutput .= "\t\t\t\t\t$textdata;\n";
				if (substr($textdata, 0, 2) == "if")	{
					$dataOutput .= "\t\t\t\t\telse loopIsValid = false; \n";
				} 
				$tLength = strlen($textdata);
				if ($acceptStateTextLength <= $tLength && substr($textdata, $tLength - $acceptStateTextLength, $acceptStateTextLength) == $acceptStateText)	{
					$acceptStateCode++;
					$dataOutput .= "\t\t\t\t\tacceptcode = $acceptStateCode; \n";
				}
			} else {
				//>1
				$numberOfIfBlocks = 0;
				$numberOfElseBlocks = 0; //can-not-exceed 1
				$elseBlockText = ""; //assign-not-append
				
				$isAcceptState = false;

				for ($i=0; $i<$len; $i++)	{
					$textdata = $currentStateTableBlock[$i];

					if (substr($textdata, 0, 2) == "if")	{
						$numberOfIfBlocks++;
						if ($numberOfIfBlocks == 1) $dataOutput .= "\t\t\t\t\t$textdata;\n";
						else $dataOutput .= "\t\t\t\t\telse $textdata;\n";
					} else {
						$numberOfElseBlocks++;
						if ($numberOfElseBlocks > 1) die("Error in state $currentState: , Multiple *** declaration from the same state not allowed");
						$elseBlockText = $textdata;
					}

					$tLength = strlen($textdata);
					if (! $isAcceptState && $acceptStateTextLength <= $tLength && substr($textdata, $tLength - $acceptStateTextLength, $acceptStateTextLength) == $acceptStateText)	{
						$isAcceptState = true;
					}
				}

				if ($numberOfElseBlocks == 1) $dataOutput .= "\t\t\t\t\telse $elseBlockText;\n";
				else $dataOutput .= "\t\t\t\t\telse loopIsValid = false;\n";

				if ($isAcceptState)	{
					$acceptStateCode++;
					$dataOutput .= "\t\t\t\t\tacceptcode = $acceptStateCode; \n";
					
				}

			}
			//I h
			$dataOutput .= "\t\t\t\t\tbreak;\n";
		}
		//switch block 
		$dataOutput = "\t\t\tswitch (state) {\n$dataOutput\n\t\t\t}";
		//Comment for lookahead
		$dataOutput = "\t\t\t//lookahead= read(); You must update lookahead here\n".$dataOutput;
		//do-while
		$dataOutput = "\t\tdo {\n $dataOutput \n\t\t} while(loopIsValid && state != $finishState);";
		//method -- void stateChange() variable declarations
		$dataOutput = "\t\tboolean loopIsValid = true;\n".$dataOutput;
		$dataOutput = "\t\tint acceptcode = -1;\n".$dataOutput;
		$dataOutput = "\t\tint state = 0;\n".$dataOutput;
		//method -- void stateChange() definition 
		$dataOutput = "\tpublic void $methodname()	{\n$dataOutput\n\t}";
		//method -- boolean is(int val) definition
		$dataOutput = "\tprivate boolean is(int val)	{\n\t\treturn lookahead == val;\n\t}\n\n".$dataOutput;
		
		//class variables 
		$dataOutput = "\tprivate int lookahead = -1;\n".$dataOutput;
		//class definition
		$dataOutput = "class $classname {\n$dataOutput\n}\n";

		return $dataOutput;
	}
}
?>
