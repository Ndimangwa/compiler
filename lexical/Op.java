package lexer;
public class Op extends Token {
	public final String op;
	public Op(int t, String s)	{
		super(t); op = new String(s);
	}
	@Override
	public String toString()	{
		return "{ tag , op } =  { " + tag + " , " + op + " }";
	} 
}