package lexer;
import java.io.*; import java.util.*;
public class Lexer {
	public int line = 1;
	public char peek = ' '; //next character
	private Hashtable words = new Hashtable();
	
	void reserve(Word w)	{ words.put(w.lexeme, w); }	
	
	public Lexer()	{
		reserve(new Word(Tag.TRUE, "true"));
		reserve(new Word(Tag.FALSE, "false"));
	}

	private Num getNumber()	throws IOException {
		int v = 0;
		do {
			v = 10 * v + Character.digit(peek, 10);
			peek = (char)System.in.read();
		} while(Character.isDigit(peek));
		return new Num(v);
	}

	private Word getID() throws IOException	{
		StringBuffer buffer1 = new StringBuffer();
		do {
			buffer1.append(peek);
			peek = (char)System.in.read();
		} while(Character.isLetterOrDigit(peek));
		String str1 = buffer1.toString();
		Word w = (Word)words.get(str1);
		if (w != null) return w;
		w = new Word(Tag.ID, str1);
		words.put(str1, w);
		return w;
	}

	private Word getComment() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();  
			switch (state) {
				case 1 :
					if (is('*')) state = 2;
					else if (is('/')) state = 5;
					else loopIsValid = false;
					break;
				case 2 :
					if (is('*')) state = 3;
					else state = 2;
					break;
				case 3 :
					if (is('/')) state = 4;
					else if (is('*')) state = 3;
					else state = 2;
					break;
				case 4 :
					state = 7;
					acceptcode = 1; 
					break;
				case 5 :
					if (is('\n')) state = 6;
					else if (is((char)-1)) state = 7; //EOF is -1
					else state = 5;
					acceptcode = 2; 
					break;
				case 6 :
					state = 7;
					acceptcode = 3; 
					break;

			} 
		} while(loopIsValid && state != 7);
		String str1 = buffer1.toString();
		return loopIsValid ? new Word(Tag.COMMENT,str1) : new Word(Tag.MALFORMED_STRING, str1);
	}
	
	private Op getAnd()	throws IOException {
		int state = 1; //Skip state 0, so we can be moving with a lookahead
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('&')) state = 2;
					else if (is('=')) state = 4;
					else state = 5;
					acceptcode = 1; 
					break;
				case 2 :
					if (is('=')) state = 3;
					else state = 5;
					acceptcode = 2; 
					break;
				case 4 :
					state = 5;
					acceptcode = 3; 
					break;
				case 3 :
					state = 5;
					acceptcode = 4; 
					break;

			} 
		} while(loopIsValid && state != 5);
		return new Op(Tag.BITWISE_AND + acceptcode - 1, buffer1.toString());
	}

	private Op getComplement() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					state = 2;
					acceptcode = 1; 
					break;

			} 
		} while(loopIsValid && state != 2);
		return new Op(Tag.COMPLEMENT, buffer1.toString());
	}

	private Op getDivide() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.DIVIDE + acceptcode - 1, buffer1.toString());
	}

	private Op getEqual() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			//lookahead= read(); You must update lookahead here
		 	buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.EQUAL + acceptcode - 1, buffer1.toString());
	}

	private Op getGreaterThan() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('>')) state = 2;
					else if (is('=')) state = 5;
					else state = 6;
					acceptcode = 1; 
					break;
				case 2 :
					if (is('>')) state = 3;
					else if (is('=')) state = 4;
					else state = 6;
					acceptcode = 2; 
					break;
				case 5 :
					state = 6;
					acceptcode = 3; 
					break;
				case 4 :
					state = 6;
					acceptcode = 4; 
					break;
				case 3 :
					state = 6;
					acceptcode = 5; 
					break;

			} 
		} while(loopIsValid && state != 6);
		return new Op(Tag.GREATER_THAN + acceptcode - 1, buffer1.toString());
	}

	private Op getLessThan()	throws IOException {
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('<')) state = 2;
					else if (is('=')) state = 4;
					else state = 5;
					acceptcode = 1; 
					break;
				case 2 :
					if (is('=')) state = 3;
					else state = 5;
					acceptcode = 2; 
					break;
				case 3 :
					state = 5;
					acceptcode = 3; 
					break;
				case 4 :
					state = 5;
					acceptcode = 4; 
					break;

			} 
		} while(loopIsValid && state != 5);
		return new Op(Tag.LESS_THAN + acceptcode - 1, buffer1.toString());
	}

	private Op getMinus()	throws IOException {
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('-')) state = 2;
					else if (is('=')) state = 3;
					else state = 4;
					acceptcode = 1; 
					break;
				case 3 :
					state = 4;
					acceptcode = 2; 
					break;
				case 2 :
					state = 4;
					acceptcode = 3; 
					break;

			} 
		} while(loopIsValid && state != 4);
		return new Op(Tag.MINUS + acceptcode - 1, buffer1.toString());
	}

	private Op getModulus()	throws IOException {
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read(); 
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.MODULUS + acceptcode - 1, buffer1.toString());
	}

	private Op getMultiply()	throws IOException {
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.MULTIPLY + acceptcode - 1, buffer1.toString());
	}

	private Op getNegate() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.NEGATE + acceptcode - 1, buffer1.toString());
	}

	private Op getOr() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
			peek = (char)System.in.read();
			switch (state) {
				case 1 :
					if (is('|')) state = 2;
					else if (is('=')) state = 4;
					else state = 5;
					acceptcode = 1; 
					break;
				case 2 :
					if (is('=')) state = 3;
					else state = 5;
					acceptcode = 2; 
					break;
				case 4 :
					state = 5;
					acceptcode = 3; 
					break;
				case 3 :
					state = 5;
					acceptcode = 4; 
					break;

			} 
		} while(loopIsValid && state != 5);
		return new Op(Tag.BITWISE_OR + acceptcode - 1, buffer1.toString());
	}

	private Op getAdd()	throws IOException {
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
		 	peek = (char)System.in.read(); 
			switch (state) {
				case 1 :
					if (is('+')) state = 2;
					else if (is('=')) state = 3;
					else state = 4;
					acceptcode = 1; 
					break;
				case 3 :
					state = 4;
					acceptcode = 2; 
					break;
				case 2 :
					state = 4;
					acceptcode = 3; 
					break;

			} 
		} while(loopIsValid && state != 4);
		return new Op(Tag.ADD + acceptcode - 1, buffer1.toString());
	}

	private Op getPower() throws IOException	{
		int state = 1;
		int acceptcode = -1;
		boolean loopIsValid = true;
		StringBuffer buffer1 = new StringBuffer();
		do {
			 //lookahead= read(); You must update lookahead here
			buffer1.append(peek);
		 	peek = (char)System.in.read(); 
			switch (state) {
				case 1 :
					if (is('=')) state = 2;
					else state = 3;
					acceptcode = 1; 
					break;
				case 2 :
					state = 3;
					acceptcode = 2; 
					break;

			} 
		} while(loopIsValid && state != 3);
		return new Op(Tag.POWER + acceptcode - 1, buffer1.toString());
	}


	public Token scan() throws IOException {
		for (;; peek = (char)System.in.read())	{
			if (peek == ' ' || peek == '\t') continue;
			else if (peek == '\n') line = line + 1;
			else break;
		}
		
		if (Character.isDigit(peek))	return getNumber();

		if (Character.isLetter(peek))	return getID();

		if (is('/'))	return getComment();

		if (is('&'))	return getAnd();

		if (is('~')) return getComplement();

		if (is('/')) return getDivide();

		if (is('=')) return getEqual();

		if (is('>')) return getGreaterThan();

		if (is('<')) return getLessThan();

		if (is('-')) return getMinus();

		if (is('%')) return getModulus();

		if (is('*')) return getMultiply();

		if (is('!')) return getNegate();

		if (is('|')) return getOr();

		if (is('+')) return getAdd();

		if (is('^')) return getPower();

		Token t = new Token(peek);
		peek = ' ';
		return t;
	}
	private boolean is(char ch)	{
		return ch == peek;
	}
}