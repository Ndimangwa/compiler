package lexer;
public class Tag {
	public final static int 
							NUM = 256,
							ID = 257,
							TRUE = 258,
							FALSE = 259,
							COMMENT = 260,

							BITWISE_AND = 270,
							LOGICAL_AND = 271,
							BITWISE_AND_ASSIGN = 272,
							LOGICAL_AND_ASSIGN = 273,

							COMPLEMENT = 280,

							DIVIDE = 290,
							DIVIDE_ASSIGN = 291,

							EQUAL = 300,
							LOGICAL_EQUAL = 301,

							GREATER_THAN = 310,
							RIGHT_SHIFT = 311,
							GREATER_THAN_OR_EQUAL = 312,
							RIGHT_SHIFT_ASSIGN = 313,
							RIGHT_SHIFT_UNASSIGNED = 314,

							LESS_THAN = 320,
							LEFT_SHIFT = 321,
							LESS_THAN_OR_EQUAL = 322,
							LEFT_SHIFT_ASSIGN = 323,

							MINUS = 330,
							MINUS_ASSIGN = 331,
							DECREMENT = 332,

							MODULUS = 340,
							MODULUS_ASSIGN = 341,

							MULTIPLY = 350,
							MULTIPLY_ASSIGN = 351,

							NEGATE = 360,
							NEGATE_COMPARE = 361,

							BITWISE_OR = 370,
							LOGICAL_OR = 371,
							BITWISE_OR_ASSIGN = 372,
							LOGICAL_OR_ASSIGN = 373,

							ADD = 380,
							ADD_ASSIGN = 381,
							INCREMENT = 382,

							POWER = 390,
							POWER_ASSIGN = 391,

							MALFORMED_STRING = 400;
}